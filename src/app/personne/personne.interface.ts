import { TypePersonne } from './typePersonne';
import { Filiere } from '../filiere/filiere.interface';
import { Modules } from '../modules/modules.interface';

export interface Personne {
  id: number;
  nom: string;
  prenom: string;
  type: TypePersonne;
  filiere?: Filiere;
  modules?: Modules;
}
