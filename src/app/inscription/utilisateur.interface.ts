export interface Utilisateur {
    id: string;
    mail: string;
    phone: string;
    birthday: Date;
    address: string;
    password: string;
    registerdate: Date;
    //formation: string;
}