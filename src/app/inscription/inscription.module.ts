import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InscriptionFormComponent } from './inscription-form/inscription-form.component';
import { ReactiveFormsModule } from '@angular/forms';
import { NgForm } from '@angular/forms';
import { RouterModule } from '@angular/router';


@NgModule({
  declarations: [
    InscriptionFormComponent
  ],
  imports: [
    CommonModule,
    ReactiveFormsModule
  ],
  exports:[
    InscriptionFormComponent
  ],
})
export class InscriptionModule { }
