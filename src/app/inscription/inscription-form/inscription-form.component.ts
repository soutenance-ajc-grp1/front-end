import { Component, Input, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { FiliereService } from '../../filiere/filiere.service';
import { Filiere } from 'src/app/filiere/filiere.interface';
import { Utilisateur } from '../utilisateur.interface';
import { Observable } from 'rxjs';
import { InscriptionService } from '../inscription.service';

@Component({
  selector: 'app-inscription-form',
  templateUrl: './inscription-form.component.html',
  styleUrls: ['./inscription-form.component.scss']
})
export class InscriptionFormComponent implements OnInit {
  filieres: Filiere[] = [];
  hidden: boolean = false;
  
  @Input() utilisateur: Utilisateur = {  
    id: '',
    mail: '',
    phone: '',
    birthday: new Date(),
    address: '',
    password: '',
    registerdate: new Date()
  }
  
  inscriptionForm: FormGroup = new FormGroup({
    lastname: new FormControl('', Validators.required),
    firstname: new FormControl('', Validators.required),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', Validators.required),
    birthday: new FormControl('', Validators.required),
    address: new FormControl('', Validators.required),
    filiere: new FormControl('', Validators.required)
  });

  constructor(
    private filiereService: FiliereService,
    private utilisateurService: InscriptionService,
    private fb: FormBuilder
    ) {
      this.formValidation();
  }

  ngOnInit(): void {
    this.filiereService.getFilieres()
      .subscribe((filieres: Filiere[]) => {
        this.filieres = filieres
      });
  }

  inscription(){
  //inscription(): Observable<Utilisateur> {
    this.hidden = true;
    console.log("incription val");
    this.utilisateur.id = this.inscriptionForm?.value.firstname[0] + 
      this.inscriptionForm?.value.lastname;
    this.utilisateur.mail = this.inscriptionForm?.value.email;
    this.utilisateur.phone = this.inscriptionForm?.value.phone;
    this.utilisateur.birthday = new Date(this.inscriptionForm?.value.birthday);
    this.utilisateur.address = this.inscriptionForm?.value.address;
    this.utilisateur.password = this.utilisateur.id;  // initial password = user id
    this.utilisateur.registerdate = new Date();
    //return this.utilisateurService.postInscription(this.utilisateur);
  }

  formValidation(){
    this.inscriptionForm = this.fb.group({
      lastname: ['', Validators.required],
      firstname: ['', Validators.required],
      mail: ['', [Validators.required, Validators.email]],
      phone: ['', Validators.required],
      birthday:['', Validators.required],
      address: ['', Validators.required]
    });
  }

}
