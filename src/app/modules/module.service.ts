import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {Modules} from 'src/app/modules/modules.interface';
import { Observable } from 'rxjs';

const API: string = 'http://localhost:8080/api/module'

@Injectable({
  providedIn: 'root'
})
export class ModuleService {

  constructor(private http: HttpClient) {}

  
  findModules(): Observable<Modules[]>{
    return this.http.get<Modules[]>(API);
  }

  deleteModule(id:number){
    return this.http.delete(API+'/'+id);
  }

}
