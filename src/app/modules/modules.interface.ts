import { Filiere } from '../filiere/filiere.interface';
import { Personne } from '../personne/personne.interface';

export interface Modules {
  id: number;
  libelle: string;
  dateDebut: string;
  dateFin: string;
  formateur?: Personne;
  filiere?: Filiere;
  nbStagiaires: number;
}
