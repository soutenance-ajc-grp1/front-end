import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModuleGestionnaireComponent } from './module-gestionnaire.component';

describe('ModuleGestionnaireComponent', () => {
  let component: ModuleGestionnaireComponent;
  let fixture: ComponentFixture<ModuleGestionnaireComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModuleGestionnaireComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModuleGestionnaireComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
