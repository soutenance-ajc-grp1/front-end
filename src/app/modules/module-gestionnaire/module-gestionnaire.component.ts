import { Component, OnInit } from '@angular/core';
import { TypePersonne } from 'src/app/personne/typePersonne';
import { ModuleService } from '../module.service';
import { Modules } from '../modules.interface';
//import { Filiere } from '../filiere.interface';

@Component({
  selector: 'module-gestionnaire',
  templateUrl: './module-gestionnaire.component.html',
  styleUrls: ['./module-gestionnaire.component.scss']
})
export class ModuleGestionnaireComponent implements OnInit {

  modules: Modules[] = [];
  moduleToDelete: Modules | null = null;
  foundModules: Modules[] = [];
  searchByLibelle: string = '';
  searchByFiliere: string = '';
  searchByFormateur: string = '';
  type: TypePersonne = TypePersonne.Gestionnaire;

  constructor(private moduleService: ModuleService) {
    this.moduleService.findModules()
      .subscribe((modules: Modules[]) => {
        this.modules = modules;
        this.foundModules = modules;
        console.log(this.modules);
      })
  }

  ngOnInit(): void {
  }

  handleSearchByLibelle(){
    this.foundModules = this.modules.filter(module => 
      module.libelle.includes(this.searchByLibelle));
  }

  handleSearchByFiliere(){
    this.foundModules = this.modules.filter(module => 
      module.filiere?.libelle.includes(this.searchByFiliere.toUpperCase()));
  }

  handleSearchByFormateur(){
    this.foundModules = this.modules.filter(module =>( 
      module.formateur?.prenom.includes(this.searchByFormateur))
      || module.formateur?.nom.includes(this.searchByFormateur));
  }

  handleClick(module: Modules){
    this.moduleService.deleteModule(module.id).subscribe();
    setTimeout(() => {
      window.location.reload();
    }, 100);
  }

}
