import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ModuleGestionnaireComponent } from './module-gestionnaire/module-gestionnaire.component';

@NgModule({
  declarations: [
    ModuleGestionnaireComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
  ],
  exports: [
    ModuleGestionnaireComponent
  ],
})
export class ModulesModule { }
