import { Modules } from '../modules/modules.interface';
import { Personne } from '../personne/personne.interface';

export interface Filiere {
  id?: number;
  libelle: string;
  modules?: Modules[];
  stagiaires: Personne[];
}
