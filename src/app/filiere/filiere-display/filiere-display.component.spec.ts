import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FiliereDisplayComponent } from './filiere-display.component';

describe('FiliereDisplayComponent', () => {
  let component: FiliereDisplayComponent;
  let fixture: ComponentFixture<FiliereDisplayComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [FiliereDisplayComponent],
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FiliereDisplayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
