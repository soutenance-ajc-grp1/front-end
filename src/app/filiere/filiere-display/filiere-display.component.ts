import { Component, Input, OnInit } from '@angular/core';
import { Modal } from 'bootstrap';
import { Filiere } from '../filiere.interface';
import { FiliereService } from '../filiere.service';
import { FiliereFormComponent } from '../filiere-form/filiere-form.component';

@Component({
  selector: 'app-filiere-display',
  templateUrl: './filiere-display.component.html',
  styleUrls: ['./filiere-display.component.scss'],
})
export class FiliereDisplayComponent implements OnInit {
  filieres: Filiere[] | null = null;

  private modal: Modal | null = null;

  constructor(private fs: FiliereService) {}

  ngOnInit(): void {
    this.getFilieres();
  }

  openAddModal(): void {
    const modalElem = document.getElementById('addFiliere')
    if (modalElem !== null){
      this.modal = new Modal(modalElem);
      this.modal.show();
    }
  }

  openModal(id: number): void {
    const modalElem = document.getElementById(id.toString())
    if (modalElem !== null){
      this.modal = new Modal(modalElem);
      this.modal.show();
    }
  }

  submit(formComponent: FiliereFormComponent): void {
    this.modal?.hide();
    formComponent.submit().subscribe((res) => {this.getFilieres();});
  }

  getFilieres(): void {
    this.fs.getFilieres().subscribe(
      (res: Filiere[])=>{
      this.filieres = res;
    });
  }

  deleteFiliere(id: number): void {
    this.fs.deleteFiliere(id).subscribe((res) => {this.getFilieres();});
  }
}
