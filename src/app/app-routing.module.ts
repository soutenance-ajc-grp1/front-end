import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { InscriptionFormComponent } from './inscription/inscription-form/inscription-form.component';
import { ModuleGestionnaireComponent } from './modules/module-gestionnaire/module-gestionnaire.component';

const routes: Routes = [];

@NgModule({
  imports: [RouterModule.forRoot([
    { path: 'inscription', component: InscriptionFormComponent},
    { path: 'module/gestion', component: ModuleGestionnaireComponent}
    ])
  ],
  exports: [RouterModule],
  })
export class AppRoutingModule { }
